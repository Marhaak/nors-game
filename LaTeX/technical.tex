\chapter{Technical Design}
\label{ch:technicalDesign}
This chapter will provide information about the technologies we used throughout the development, and what function they have in the game.


\section{Unreal Engine}
\label{sec:unrealEngine}
The game is made in Unreal Engine 4.7.x, and we decided on using the Visual Scripting language that come with the engine. This scripting language is called Blueprints. This would provide a learning goal as we hadn't used Blueprints before starting the project. After struggling in the start as we were learning the language, it got better and we're able to program fast and efficiently. The language itself is easily translated from the scripting language into C++ code. It is powerful and have advanced functionality that counterparts C++ functionality. Notable examples being Event dispatchers and latent functions, working similarly to function pointers and running a function on a spawned thread.

In the event that we needed something that didn't exist as Blueprints, we could write our own classes in C++ and have them appear as Blueprints. We found and installed a plugin that implemented RESTful API calls to a server in Blueprints. The plugin was slightly outdated, so we had to recompile the plugin with the engine version we were using at the time. So we have basic grasp on how to implement new functionality in Blueprint by a plugin.



\section{Blueprint}
\label{sec:blueprint}
In this section we will try to give a simple explanation of how some the different elements of the Blueprints system works. This won't be a complete Blueprint guide, but more of a general explanation that would help one understand the basics of the source code. 

Blueprint is the visual scripting language in Unreal Engine 4. You can use it to create fairly complicated gameplay elements and game logic. In Blueprints you don't write code as text. Instead we are using Nodes with Wires between them. There are two main differences between the different wires. There is the sequence of execution wires that are white, and the data flow wires which are colored depending on data type. Reference the Unreal Engine documentation about variables\footnote{\url{https://docs.unrealengine.com/latest/INT/Engine/Blueprints/UserGuide/Variables/index.html} - Unreal Engine documentation on variables} to see what colors are associated with what variable types.

One exception is the wire associated with event dispatchers, which you connect by a thin red wire. This wire will trigger an event when the node is called, but the event might not execute immediately, thus it doesn't represent the sequence of execution as the white wire does, but is rather a variable to a function.


\subsection{Classes}
\label{subsec:BPclasses}
In order to make a new Blueprint object in Unreal Engine, you have to extend some existing class. This is equal to a superclass in c++, java, etc. The new Blueprint will contain some basic functionality and variables as found in the selected superclass. Some of the classes work quite differently from each other. For instance, a Player Character base class will support movement and control, while a Widget Blueprint will support UI elements and touch interaction.

The most basic Blueprint class is an actor. It is simply an object that will exist in the world with a piece of code attacked to it. One can also extend own classes in a hierarchy of objects. Further information about Blueprint classes can be found in the Unreal Epic documentation on Class Blueprint\footnote{\url{https://docs.unrealengine.com/latest/INT/Engine/Blueprints/UserGuide/Types/ClassBlueprint/index.html} - Unreal Engine Documentation on Class Blueprint}.


\subsection{Variable}
\label{subsec:BPvariable}
Blueprints handle variables through getter and setter nodes. As you can see in Figure~\ref{fig:BPvariable} you can get the variable and assign a new value to it. In the figure, the first thing that happens is that we are assigning NewInt to the value of NewInt (Done as an example with little practical use). Next, we assign NewBool to true. Given that we are not using another variable as input, it counterparts hardcoding in a value in C++. The last node that you see is a branch node taking the NewBool variable as input. It works like an if/else statement and will either execute the code associated with the True or False execution outputs.

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{Images/BPvariables}
\caption[Variables in Blueprint] {Variables in Blueprint}
\label{fig:BPvariable}
\end{figure}

\subsection{Function}
\label{subsec:BPfunction}
Functions in blueprint is used the same way as you would use a function in any other language. You are able to have many different input parameters, and many different return values. The example in Figure~\ref{fig:BPfunction} have one parameter and two return values. What it simply does is to check if the InputInt is below 10, and it also adds 5 to the value of what InputInt is, and returns the true/false for the check and the new value.

A compiled function appear as a node accessible elsewhere in the Blueprint. One notable unique feature is the "Target" pin on the New Function node when we are calling it. This defaults to "self" and refer to which Blueprint you are calling the function on. By default, the function is called on the blueprint that is doing the calling. But one could also call the inherited Apply Damage function from within a Player Character, and set target another Player Character. As such, the target is the one that would take damage.

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{Images/BPfunction}
\caption[Function in Blueprint] {An example of a function and how to call it.}
\label{fig:BPfunction}
\end{figure}

\subsection{Event}
\label{BPevent}
An event is an entry point for code. Mainly it's used for responding to engine events such as collision, and replication across multiple computers. Events can utilize latent functions and delay, they do not return anything to the caller and as such stand separate from their caller. You call an event much like a function, except you are not guaranteed it will execute and return immediately. All events, including custom events, in a blueprint exist in the same space. This mean that multiple events can work on identical code, and even merge execution path.


\subsection{Macro}
\label{BPmacro}
A macro differ from functions and events in that upon compile, the content of the macro get copied in and replace the macro node where it's used. This mean that a macro that's used multiple places, get compiled multiple times. Because a macro is copied in on compile, it will face the limitations of it's parent execution. If it's called upon from a function, it can't contain latent functions or delay. If it's called from within an event, it can. A macro in reality doesn't return any values, despite how you can give it an output execution path that pass along variables. It would be more along the lines of creating temporary values, and then keeping a reference to them after the macro have executed.


\subsection{Colapsed Code}
\label{BPcolapsedFunc}
Especially useful for handling complexity is the ability in Blueprints to collapse pieces of code into what appears as one node. This is done simply by marking some code, and if it fit the criteria you can right click and collapse code. This allow you to easily manage sections of code, because they only appear as one node, making it easy to follow the path of execution. Alternatively you can collapse it to a function. This will create a function of the code you had selected and can be called like any other function.


\subsection{Actor Component}
\label{subsec:BPactorComponent}
An Actor Component is something that was introduced in Unreal Engine 4.7. Basically an Actor Component is some predefined behavior that you have made. You can add it as a component to your Blueprint and the behavior you made will now work as if part of the Blueprint.\footnote{\url{https://wiki.unrealengine.com/Videos/Player?series=PLZlv_N0_O1gYeJX3xX44yzOb7_kTS7FPM&video=qr4ZjieAQKY} - Video explaining actor component}

\subsection{Widget}
\label{subsec:BPwidget}
As mentioned in subsection~\ref{subsec:BPclasses} there are many different Blueprints and they behave differently. The Widget blueprint is a particularly interesting class to extend.

Unreal Engine 4 has a visual UI designing tool called \textbf{Unreal Motion Graphics UI Designer (UMG)} and at the core of UMG are widgets. A widget is something that defines different HUD elements such as buttons, sliders, check boxes, \textit{etc}. These elements are designed using the designer tab in the editor window. The buttons are added from the palette panel. For instance, when executing code as a result of touch events on buttons, you first enable an "On Click Event" for the particular button you have selected. The editor then change to the graph tab where you have the Event node to start the execution.

Figure~\ref{fig:BPwidgetDesigner} is an example on how you can arrange the different elements in the designer tab in the editor. The image is taken from our game, and consist of several widget components to make up the widget. For details about what the different widgets are in this picture, see Figure~\ref{fig:GUI} in section~\ref{sec:gui}.

\begin{figure}[h]
\centering
\includegraphics[width=1\textwidth]{Images/BPwidgetDesigner}
\caption[Widget Designer] {An example of how you may arrange your HUD in a widget.}
\label{fig:BPwidgetDesigner}
\end{figure}


\subsection{Construction Script}
\label{subsec:constructionScript}
The construction script is a special section of Blueprints, and some Blueprints, such as Level Blueprint, doesn't have any construction script. A construction script allow us to run snippets of code as we construct the object. This applies for editing in the editor as well. An example is having a flower object that when placed, create several flower meshes of varying size, rotation and position. The flower object could potentially get it's position and determine the color of the flowers based on that.

\section{Version Control}
\label{sec:versionControl}
We had decided on using Git as our version control, and decided on Bitbucket rather than Github for hosting our repository. These are the main sites for hosting Git repositories. Bitbucket allow closed repositories and have close integration with Confluence and Jira, which we intended to use as well. Github also offer closed repositories as part of a free student pack, and because we didn't use Jira or Confluence as active as we should, there is no real advantage one way or another.




\section{Tracking and documentation}
\label{sec:trackingAndDocumentation}
For time tracking and daily reports we used Toggl and Google Documents. Google Documents would allow all of us to join and see what was written in the short reports as well as write themselves if something was missing or wrong. Toggl is a simple time tracker that let us specify what we're working on, when we were working, and on what project we were working.

Confluence and Jira was used briefly as wiki documentation and issue tracking. It was never a main focus and as such Jira was rarely used other than a burst of activity. Confluence was set up as a simple game wiki describing gameplay, abilities and enemies. It does not contain any code, but rather basic information, background story and lore that one would be interested in as a player.




\section{Blender}
\label{sec:blender}

\begin{quote}
``Blender is the free and open source 3D creation suite. It supports the entirety of the 3D pipeline. —modeling, rigging, animation, simulation, rendering, compositing and motion tracking, even video editing and game creation. Advanced users employ Blender’s API for Python scripting to customize the application and write specialized tools; often these are included in Blender’s future releases. Blender is well suited to individuals and small studios who benefit from its unified pipeline and responsive development process.''\cite{BlenderAbout}
\end{quote}


Blender is released under the GNU General Public License (GPL, or “free software”).\cite{BlenderLicense}

