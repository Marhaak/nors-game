\select@language {english}
\contentsline {chapter}{Preface}{iii}{section*.2}
\contentsline {chapter}{Contents}{iv}{section*.4}
\contentsline {chapter}{List of Figures}{v}{section*.6}
\contentsline {chapter}{List of Tables}{vi}{section*.8}
\contentsline {chapter}{\numberline {1}Introduction}{1}{section*.9}
\contentsline {chapter}{\numberline {2}Packages}{2}{section*.10}
\contentsline {section}{\numberline {2.1}Packages Used by gucthesis}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Other Relevant Packages}{2}{section.2.2}
\contentsline {chapter}{\numberline {3}Structural Elements}{3}{section*.11}
\contentsline {section}{\numberline {3.1}Page Layout}{3}{section.3.1}
\contentsline {section}{\numberline {3.2}Fonts}{3}{section.3.2}
\contentsline {section}{\numberline {3.3}Sectioning Commands}{3}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}The subsection}{3}{subsection.3.3.1}
\contentsline {subsubsection}{The subsubsection}{3}{section*.12}
\contentsline {paragraph}{The paragraph}{3}{section*.13}
\contentsline {section}{\numberline {3.4}Floats (Figures and Tables)}{3}{section.3.4}
\contentsline {section}{\numberline {3.5}Quotes}{4}{section.3.5}
\contentsline {section}{\numberline {3.6}Lists}{4}{section.3.6}
\contentsline {section}{\numberline {3.7}Bibliographic References}{5}{section.3.7}
\contentsline {chapter}{Bibliography}{6}{section*.15}
\contentsline {chapter}{\numberline {A}Meetings}{7}{section*.16}
