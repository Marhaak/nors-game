// Copyright 2014 Vladimir Alyamkin. All Rights Reserved.

#include "VaRestPrivatePCH.h"

class FVaRest : public IVaRest
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override
	{
		// @HACK Force classes to be compiled on shipping build
		UVaRestJsonObject::StaticClass();
		UVaRestJsonValue::StaticClass();
		UVaRestRequestJSON::StaticClass();
		UVaRestParseManager::StaticClass();
	}

	virtual void ShutdownModule() override
	{

	}
};

IMPLEMENT_MODULE( FVaRest, VaRest )

DEFINE_LOG_CATEGORY(LogVaRest);
