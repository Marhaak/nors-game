<?php

	// For debugging
	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);
	
	// Require the database connection and make a global connection
	require "Include/PHP/PDO.php";
	global $pdo;


	// Gets the data and parse it to a JSON input.
	$input = file_get_contents('php://input');
	$JSONinput = json_decode($input, true);

	// JSON Response, encoded at the end.
	$response;

	// Missing username
	if ($JSONinput[Username] == null){
		$response["Message"] = "Missing username";
		$response["Success"] = false;
		
	// Missing password
	} else if ($JSONinput[Password] == null){
		$response["Message"] = "Missing password";
		$response["Success"] = false;
	
	// Username and Password present
	} else {
		

		// Format the input data, uses Username as "salt" for password
		$Username = format( $JSONinput[Username] );
		$Password = format( hash('sha256', ($JSONinput[Username] .= $JSONinput[Password]) ) );
		
		
		// If we should register new user
		if($JSONinput[Register] == true){
		
			// Missing email
			if ($JSONinput[Email] == null){
				$response["Message"] = "Missing email";
				$response["Success"] = false;
				
			} else {
				// Format the email input
				$Email = format( $JSONinput[Email] );
				
			
				// Check that username isn't taken
				// preparing statement
				$stmt = $pdo->prepare("SELECT Username
									   FROM NORS_User 
									   WHERE Username = :username
									   ORDER BY ID DESC
									   LIMIT 1");
						   
				// Executing statement with search parameter.
				$stmt->execute(array(':username' => $Username));
		
		
				// Check for unique username
				$counter = 0;
				foreach ($stmt as $row){
					$counter++;
				}
				
				// If username is taken
				if($counter != 0){
					// Set response
					$response["Message"] = "Username is Taken";
					$response["Success"] = false;
							
				// If it is unique
				} else {
			
					//Try inserting new username
					try{
						//perparing statement for security.
						$insert = $pdo->prepare('INSERT INTO NORS_User (Username, Password, Email)
															VALUES (:username, :password, :email)');
					
						//assigning the correct values.
						$insert->execute(array( ':username' => $Username,
												':password' => $Password,
												':email' 	=> $Email));
						
						
						//Insert successful
						if ($pdo->lastInsertId() != 0){
							//Set response and return
							$response["Success"] = true;
							$response["Message"] = "Successful Register";
						}
				
					} catch(Exception $e){
						//Set response and return
						$response["Success"] = false;
						$response["Message"] = "Something went wrong on SQL";
					}
				}
			}
			
		// IF we should simply login
		} else {
			// Check that username isn't taken
			// preparing statement
			$stmt = $pdo->prepare("SELECT Username, Password
									 FROM NORS_User 
									 WHERE Username = :username
									 ORDER BY ID DESC
									 LIMIT 1");
							   
			// Executing statement with search parameter.
			$stmt->execute(array(':username' => $Username));
			
			$counter = 0;
			// Check for unique username
			foreach ($stmt as $row){
				$counter++;
				
				// Assuming the password is valid
				if($Password == $row["Password"]){
						
					// Set response and return
					$response["Message"] = "Login Successful";
					$response["Success"] = true;
				} else {
					$response["Message"] = "Invalid password";
					$response["Success"] = false;
				}
			}
			
			// No user found by that username
			if($counter == 0){
				$response["Message"] = "User not found";
				$response["Success"] = false;
			}
		
		}
	}








	//Encode and echo response
	echo json_encode($response);
	
	
	
	//Removes spaces
	function format($input){
		return str_replace(' ', '', $input);
	}
	
	
	
?>