<?php

	// For debugging
	// error_reporting(E_ALL);
	// ini_set('display_errors', 1);
	
	//Gets the data and parse it to a JSON input.
	$input = file_get_contents('php://input');
	$JSONinput = json_decode($input, true);

	$response;

	//Echo several different values
	if ($JSONinput[Username] == null){
		$response["Message"] = "Missing username";
		$response["Success"] = false;
		
	} else if ($JSONinput[Password] == null){
		$response["Message"] = "Missing password";
		$response["Success"] = false;
	
	} else {
		$response["Message"] = "Both username and password present";
		$response["Success"] = true;
	}

	echo json_encode($response);
?>